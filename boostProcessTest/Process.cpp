#include "Process.h"

Process::Process(io_context &_io) : m_pipe(_io), m_timer(_io)
{
}

std::error_code Process::create_async(const std::string &_cmd, const std::string &_input, std::string &_output, yield_context& _yield)
{
	bp::opstream in;
	m_child = std::make_shared<bp::child>(_cmd, bp::std_out > m_pipe, bp::std_in < in, m_ret);

	if (m_child != nullptr && m_child->running())
	{
		if (_input.size() > 0)
		{
			in << _input;
			in.flush();
			in.pipe().close();
		}

		boost::asio::spawn(m_timer.get_io_context(), std::bind(&Process::doTimeout, this, std::placeholders::_1));
		m_timer.expires_from_now(std::chrono::seconds(m_timeout));

		while (m_child->running())
		{
			std::vector<char> buf(4096);
			::boost::system::error_code ec;
			int size = ::boost::asio::async_read(m_pipe, boost::asio::buffer(buf), _yield[ec]);
			if (size > 0)
			{
				_output += std::string(buf.begin(), buf.begin() + size);
			}
		}
	}
	return m_ret;
}

void Process::doTimeout(yield_context _yield)
{
	if (m_child != nullptr)
	{
		while (m_child->running())
		{
			boost::system::error_code ec;
			m_timer.async_wait(_yield[ec]);
			if (m_timer.expires_from_now() <= std::chrono::seconds(0))
			{
				if (m_child != nullptr && m_child->running())
				{
					m_child->terminate(m_ret);	
				}
			}
		}
	}
}

Process::~Process()
{
}
